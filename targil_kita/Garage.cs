﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageSpace
{
    class Garage
    {
        private List<Car> m_cars = new List<Car>();

        public void AddCar(Car c)
        {
            if (c == null)
                throw new ArgumentException("cannot add <null> car to garage, dah?!");
            
            m_cars.Add(c);
        }
    }
}
