﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDilema
{
    class Waiter
    {
        private KitchenMan m_kitchen_manager;

        public Waiter()
        {
            m_kitchen_manager = new KitchenMan();
        }

        public void ExecuteOrder(string order)
        {
            Console.WriteLine($"Waiter: handling order {order}");
            // scenario 1 - waiter can handle the problem
            //try
            //{
            //    m_kitchen_manager.MakeDish(order);
            //}
            //catch (OutOfPitaException ex)
            //{
            //    Console.WriteLine("Waiter saw no pita, so waiter will give the humus without pita and say 'sorry'");
            //}

            m_kitchen_manager.MakeDish(order);
        }

        internal void CancelOrder()
        {
            Console.WriteLine("order is cancelled ... money back and free dessert");
        }
    }
}
