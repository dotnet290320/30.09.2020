﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDilema
{
    class Program
    {
        static void Main(string[] args)
        {
            Waiter matta_de = new Waiter();

            // scenario where customer solves the problem
            //try
            //{
            //    matta_de.ExecuteOrder(Globals.HUMUS_DISH);
            //}
            //catch (OutOfPitaException ex)
            //{
            //    matta_de.CancelOrder();
            //}

            // scenario 2 - customer gets the outter exception 
            try
            {
                matta_de.ExecuteOrder(Globals.HUMUS_DISH);
            }
            catch (KitchenException ex)
            {
                Console.WriteLine("The kitchen has problems ... ???");
            }

            try
            {
                throw new ArithmeticException("1", new DivideByZeroException("2", new OverflowException("3")));
            }
            catch (Exception ex2)
            {
                Console.WriteLine();
            }
        }
    }
}
