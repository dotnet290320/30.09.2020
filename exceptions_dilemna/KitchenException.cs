﻿using System;
using System.Runtime.Serialization;

namespace ExceptionDilema
{
    [Serializable]
    internal class KitchenException : Exception
    {
        public KitchenException()
        {
        }

        public KitchenException(string message) : base(message)
        {
        }

        public KitchenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KitchenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}