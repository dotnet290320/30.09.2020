﻿using System;
using System.Runtime.Serialization;

namespace ExceptionDilema
{
    [Serializable]
    internal class OutOfPitaException : Exception
    {
        public OutOfPitaException()
        {
        }

        public OutOfPitaException(string message) : base(message)
        {
        }

        public OutOfPitaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OutOfPitaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}