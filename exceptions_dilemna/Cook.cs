﻿using System;

namespace ExceptionDilema
{
    class Cook
    {
        public void PrepareDish(string order)
        {
            if (order == Globals.HUMUS_DISH)
            {
                if (KitchenStock.Pita > 0)
                {
                    Console.WriteLine($"{order} Dish is ready...");
                    KitchenStock.Pita--;
                }
                else
                {
                    // we have 0 pita , what to do ?
                    // 1 -- cook throws and cook catch. some scenarios cook catch and rethrow!
                    //try
                    //{
                    //    throw new OutOfPitaException("...");
                    //}
                    //catch (OutOfPitaException ex)
                    //{
                    //    Console.WriteLine("Out of pita exception ... going to maafia ...");
                    //    Console.WriteLine($"{order} Dish is ready...");
                    //    if (Maafia.Pita == 0)
                    //        throw;
                    //    Maafia.Pita--;
                    //}

                    // 2 -- cook throws and cannot solve the problem .....
                    // 2.1 throw only
                    Console.WriteLine("Cook: Making humus wihtout pita");
                    throw new OutOfPitaException("...");

                    // 2.2. throw , then put some help, then throw again
                    //try
                    //{
                    //    throw new OutOfPitaException("...");
                    //}
                    //catch (OutOfPitaException ex)
                    //{
                    //    Console.WriteLine("Out of pita exception ... going to maafia ...");
                    //    throw;  
                    //}
                }
            }
        }
    }
}