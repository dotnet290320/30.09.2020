﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDilema
{
    class KitchenMan
    {
        private Cook m_cook;
        public KitchenMan()
        {
            m_cook = new Cook();
        }
        public void MakeDish(string order)
        {
            // scenario 1 - kitchen manager can solve the problem
            //try
            //{
            //m_cook.PrepareDish(order);
            //}
            //catch (OutOfPitaException ex)
            //{
            // does KitchenMan can solve this problem?
            //  Console.WriteLine("Kitchen Manager: Taking our order without pita. discount 50%");
            //}

            // scenario 2 - kitchen man cannot solve
            try
            {
                m_cook.PrepareDish(order);
            }
            catch (OutOfPitaException ex)
            {
                Console.WriteLine("kitchen manager goes to refill pita");
                throw new KitchenException("Oops ... something went wrong in the kitchen", ex);
            }
        }
    }
}
