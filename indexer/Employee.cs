﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerDemo
{
    class Employee
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Salary { get; set; }
        public override string ToString()
        {
            return $"{base.ToString()} {Id} {Name} {Salary}";
        }
    }
}
